# Copyright 2017 Dimitri Capitaine
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import pyphoenix
import urlparse
import urllib


from sqlalchemy.engine.default import DefaultDialect
from sqlalchemy.sql.compiler import DDLCompiler, SQLCompiler, _CompileLabel, selectable, functions, OPERATORS, IdentifierPreparer
from sqlalchemy.exc import CompileError
from sqlalchemy.sql import elements, operators

from sqlalchemy import types
from errors import TenantError
from sqlalchemy.types import INTEGER, BIGINT, SMALLINT, VARCHAR, CHAR, \
    FLOAT, DATE, BOOLEAN, DECIMAL, TIMESTAMP, TIME, VARBINARY


class IdentifierPreparerCustom(IdentifierPreparer):

    def __init__(self, dialect, initial_quote='"',
                 final_quote=None, escape_quote='"', omit_schema=False):
        super(IdentifierPreparerCustom, self).__init__(dialect, initial_quote, final_quote, escape_quote, omit_schema)

    def quote(self, ident, force=None):
        return self.quote_identifier(ident) if self._requires_quotes(ident) or force else ident

    def _requires_quotes(self, value):
        lc_value = value.lower()
        return (lc_value in self.reserved_words
                or value[0] in self.illegal_initial_characters
                or (lc_value != value))

    def format_label(self, label, name=None):
        return self.quote(name or label.name)

    def quote_identifier(self, value):
        """Quote an identifier.

        Subclasses should override this to provide database-dependent
        quoting behavior.
        """

        return self.initial_quote + \
            self._escape_identifier(value) + \
            self.final_quote

    def _escape_identifier(self, value):
        """Escape an identifier.

        Subclasses should override this to provide database-dependent
        escaping behavior.
        """

        return value.replace(self.escape_quote, self.escape_to_quote)


class PhoenixSQLCompiler(SQLCompiler):

    def __init__(self, dialect, statement, column_keys=None, inline=False, **kwargs):
        super(PhoenixSQLCompiler, self).__init__(dialect, statement, column_keys, inline, **kwargs)
        self.preparer = IdentifierPreparerCustom(dialect)

    def _compose_select_body(
            self, text, select, inner_columns, froms, byfrom, kwargs):
        text += ', '.join(inner_columns)

        if froms:
            text += " \nFROM "

            if select._hints:
                text += ', '.join(
                    [f._compiler_dispatch(self, asfrom=True,
                                          fromhints=byfrom, **kwargs)
                     for f in froms])
            else:
                text += ', '.join(
                    [f._compiler_dispatch(self, asfrom=True, **kwargs)
                     for f in froms])
        else:
            text += self.default_from()

        if select._whereclause is not None:
            t = select._whereclause._compiler_dispatch(self, **kwargs)
            if t:
                text += " \nWHERE " + t

        if select._group_by_clause.clauses:
            group_by = select._group_by_clause._compiler_dispatch(
                self, **kwargs)
            if group_by:
                text += " GROUP BY " + group_by

        if select._having is not None:
            t = select._having._compiler_dispatch(self, **kwargs)
            if t:
                text += " \nHAVING " + t

        if select._order_by_clause.clauses:
            text += self.order_by_clause(select, **kwargs)

        if (select._limit_clause is not None or
                select._offset_clause is not None):
            text += self.limit_clause(select, **kwargs)

        if select._for_update_arg is not None:
            text += self.for_update_clause(select, **kwargs)

        return text

    def _label_select_column(self, select, column,
                             populate_result_map,
                             asfrom, column_clause_args,
                             name=None,
                             within_columns_clause=True):
        """produce labeled columns present in a select()."""

        if column.type._has_column_expression and populate_result_map:
            col_expr = column.type.column_expression(column)
            add_to_result_map = lambda keyname, name, objects, type_: \
                self._add_to_result_map(
                    keyname, name,
                    (column,) + objects, type_)
        else:
            col_expr = column
            if populate_result_map:
                add_to_result_map = self._add_to_result_map
            else:
                add_to_result_map = None

        if not within_columns_clause:
            result_expr = col_expr
        elif isinstance(column, elements.Label):
            if col_expr is not column:
                result_expr = _CompileLabel(
                    col_expr,
                    column.name,
                    alt_names=(column.element,)
                )
            else:
                result_expr = _CompileLabel(
                    col_expr,
                    column.name,
                    alt_names=(column.element,)
                )

        elif select is not None and name:
            result_expr = _CompileLabel(
                col_expr,
                name,
                alt_names=(column._key_label,)
            )

        elif \
            asfrom and \
            isinstance(column, elements.ColumnClause) and \
            not column.is_literal and \
            column.table is not None and \
                not isinstance(column.table, selectable.Select):
            result_expr = _CompileLabel(col_expr,
                                        elements._as_truncated(column.name),
                                        alt_names=(column.key,))
        elif (
            not isinstance(column, elements.TextClause) and
            (
                not isinstance(column, elements.UnaryExpression) or
                column.wraps_column_expression
            ) and
            (
                not hasattr(column, 'name') or
                isinstance(column, functions.Function)
            )
        ):
            result_expr = _CompileLabel(col_expr, column.anon_label)
        elif col_expr is not column:
            # TODO: are we sure "column" has a .name and .key here ?
            # assert isinstance(column, elements.ColumnClause)
            result_expr = _CompileLabel(col_expr,
                                        elements._as_truncated(column.name),
                                        alt_names=(column.key,))
        else:
            result_expr = _CompileLabel(col_expr,
                                        elements._as_truncated(column.name),
                                        alt_names=(column.key,))

        column_clause_args.update(
            within_columns_clause=within_columns_clause,
            add_to_result_map=add_to_result_map
        )
        return result_expr._compiler_dispatch(
            self,
            **column_clause_args
        )

    def format_label(self, label, name=None):
        return self.preparer.quote_identifier(name or label.name)

    def visit_label(self, label,
                    add_to_result_map=None,
                    within_label_clause=False,
                    within_columns_clause=False,
                    render_label_as_label=None,
                    **kw):
        # only render labels within the columns clause
        # or ORDER BY clause of a select.  dialect-specific compilers
        # can modify this behavior.
        render_label_with_as = (within_columns_clause and not
                                within_label_clause)
        render_label_only = render_label_as_label is label

        if render_label_only or render_label_with_as:
            if isinstance(label.name, elements._truncated_label):
                labelname = self._truncated_identifier("colident", label.name)
            else:
                labelname = label.name

        if render_label_with_as:
            if add_to_result_map is not None:
                add_to_result_map(
                    labelname,
                    label.name,
                    (label, labelname, ) + label._alt_names,
                    label.type
                )

            return label.element._compiler_dispatch(
                self, within_columns_clause=True,
                within_label_clause=True, **kw) + \
                OPERATORS[operators.as_] + \
                self.format_label(label, labelname)
        elif render_label_only:
            return self.format_label(label, labelname)
        else:
            return label.element._compiler_dispatch(
                self, within_columns_clause=False, **kw)

    def visit_select(self, select, asfrom=False, parens=True,
                     fromhints=None,
                     compound_index=0,
                     nested_join_translation=False,
                     select_wraps_for=None,
                     lateral=False,
                     **kwargs):

        needs_nested_translation = \
            select.use_labels and \
            not nested_join_translation and \
            not self.stack and \
            not self.dialect.supports_right_nested_joins

        if needs_nested_translation:
            transformed_select = self._transform_select_for_nested_joins(
                select)
            text = self.visit_select(
                transformed_select, asfrom=asfrom, parens=parens,
                fromhints=fromhints,
                compound_index=compound_index,
                nested_join_translation=True, **kwargs
            )

        toplevel = not self.stack
        entry = self._default_stack_entry if toplevel else self.stack[-1]

        populate_result_map = toplevel or \
            (
                compound_index == 0 and entry.get(
                    'need_result_map_for_compound', False)
            ) or entry.get('need_result_map_for_nested', False)

        # this was first proposed as part of #3372; however, it is not
        # reached in current tests and could possibly be an assertion
        # instead.
        if not populate_result_map and 'add_to_result_map' in kwargs:
            del kwargs['add_to_result_map']

        if needs_nested_translation:
            if populate_result_map:
                self._transform_result_map_for_nested_joins(
                    select, transformed_select)
            return text

        froms = self._setup_select_stack(select, entry, asfrom, lateral)

        column_clause_args = kwargs.copy()
        column_clause_args.update({
            'within_label_clause': False,
            'within_columns_clause': False
        })

        text = "SELECT "  # we're off to a good start !

        if select._hints:
            hint_text, byfrom = self._setup_select_hints(select)
            if hint_text:
                text += hint_text + " "
        else:
            byfrom = None

        if select._prefixes:
            text += self._generate_prefixes(
                select, select._prefixes, **kwargs)

        text += self.get_select_precolumns(select, **kwargs)
        # the actual list of columns to print in the SELECT column list.
        inner_columns = [
            c for c in [
                self._label_select_column(
                    select,
                    column,
                    populate_result_map, asfrom,
                    column_clause_args,
                    name=name)
                for name, column in select._columns_plus_names
            ]
            if c is not None
        ]

        if populate_result_map and select_wraps_for is not None:
            # if this select is a compiler-generated wrapper,
            # rewrite the targeted columns in the result map

            translate = dict(
                zip(
                    [name for (key, name) in select._columns_plus_names],
                    [name for (key, name) in
                     select_wraps_for._columns_plus_names])
            )

            self._result_columns = [
                (key, name, tuple(translate.get(o, o) for o in obj), type_)
                for key, name, obj, type_ in self._result_columns
            ]

        text = self._compose_select_body(
            text, select, inner_columns, froms, byfrom, kwargs)

        if select._statement_hints:
            per_dialect = [
                ht for (dialect_name, ht)
                in select._statement_hints
                if dialect_name in ('*', self.dialect.name)
            ]
            if per_dialect:
                text += " " + self.get_statement_hint_text(per_dialect)

        if self.ctes and toplevel:
            text = self._render_cte_clause() + text

        if select._suffixes:
            text += " " + self._generate_prefixes(
                select, select._suffixes, **kwargs)

        self.stack.pop(-1)

        if (asfrom or lateral) and parens:
            return "(" + text + ")"
        else:
            return text

    def visit_column(self, column, add_to_result_map=None,
                     include_table=True, **kwargs):

        self.preparer = IdentifierPreparerCustom(self.dialect)

        name = orig_name = column.name
        if name is None:
            name = self._fallback_column_name(column)

        is_literal = column.is_literal
        if not is_literal and isinstance(name, elements._truncated_label):
            name = self._truncated_identifier("colident", name)

        if add_to_result_map is not None:
            add_to_result_map(
                name,
                orig_name,
                (column, name, column.key),
                column.type
            )

        if is_literal:
            name = self.escape_literal_column(name)
        else:
            name = self.preparer.quote(name, force=True)

        table = column.table
        if table is None or not include_table or not table.named_with_column:
            return name
        else:
            effective_schema = self.preparer.schema_for_object(table)

            if effective_schema:
                schema_prefix = self.preparer.quote_schema(
                    effective_schema) + '.'
            else:
                schema_prefix = ''
            tablename = table.name
            if isinstance(tablename, elements._truncated_label):
                tablename = self._truncated_identifier("alias", tablename)

            return schema_prefix + \
                self.preparer.quote(tablename) + \
                "." + name


class PhoenixDDLCompiler(DDLCompiler):

    def visit_primary_key_constraint(self, constraint):
        if constraint.name is None:
            raise CompileError("can't create primary key without a name")
        return DDLCompiler.visit_primary_key_constraint(self, constraint)


class PhoenixDialect(DefaultDialect):

    name = "phoenix"

    driver = "pyphoenix"

    ddl_compiler = PhoenixDDLCompiler
    statement_compiler = PhoenixSQLCompiler

    @classmethod
    def dbapi(cls):

        return pyphoenix

    def create_connect_args(self, url):
        phoenix_url = urlparse.urlunsplit(urlparse.SplitResult(
            scheme='http',
            netloc='{}:{}'.format(url.host, url.port or 8765),
            path='/',
            query=urllib.urlencode(url.query),
            fragment='',
        ))
        return [phoenix_url], {'autocommit': True}

    def do_close(self, dbapi_connection):
        dbapi_connection and dbapi_connection.close()

    def do_rollback(self, dbapi_conection):
        pass

    def do_commit(self, dbapi_conection):
        pass

    def has_table(self, connection, table_name, schema=None):
        if schema is None:
            query = "SELECT 1 FROM system.catalog WHERE table_name = ? LIMIT 1"
            params = [table_name, ]
        else:
            query = "SELECT 1 FROM system.catalog WHERE table_name = ? AND TABLE_SCHEM = ? LIMIT 1"
            params = [table_name.upper(), schema.upper()]
        return connection.execute(query, params).first() is not None

    def get_schema_names(self, connection, **kw):
        query = "SELECT DISTINCT TABLE_SCHEM FROM SYSTEM.CATALOG"
        return [row[0] for row in connection.execute(query)]

    def get_table_names(self, connection, schema=None, **kw):
        if schema is None:
            query = "SELECT DISTINCT table_name FROM SYSTEM.CATALOG"
            params = []
        else:
            query = "SELECT DISTINCT table_name FROM SYSTEM.CATALOG WHERE TABLE_SCHEM = ? "
            params = [schema.upper()]
        return [row[0] for row in connection.execute(query, params)]

    def get_columns(self, connection, table_name, schema=None, **kw):
        if schema is None:
            query = "SELECT COLUMN_NAME,  DATA_TYPE, NULLABLE " \
                    "FROM system.catalog " \
                    "WHERE table_name = ? " \
                    "AND COLUMN_NAME  IS NOT NULL " \
                    "ORDER BY ORDINAL_POSITION"
            params = [table_name]
        else:
            query = "SELECT COLUMN_NAME, DATA_TYPE, NULLABLE " \
                    "FROM system.catalog " \
                    "WHERE TABLE_SCHEM = ? " \
                    "AND table_name = ? " \
                    "AND COLUMN_NAME  IS NOT NULL " \
                    "ORDER BY ORDINAL_POSITION"
            params = [schema.upper(), table_name]

        # get all of the fields for this table
        c = connection.execute(query, params)
        cols = []
        # first always none
        c.fetchone()
        while True:
            row = c.fetchone()
            if row is None:
                break
            name = row[0]
            col_type = COLUMN_DATA_TYPE[row[1]]
            nullable = row[2] == 1 if True else False

            col_d = {
                'name': name,
                'type': col_type,
                'nullable': nullable,
                'default': None
            }

            cols.append(col_d)
        return cols

    def get_pk_constraint(self, conn, table_name, schema=None, **kw):
        return []

    def get_foreign_keys(self, conn, table_name, schema=None, **kw):
        return []

    def get_indexes(self, conn, table_name, schema=None, **kw):
        return []


class PhoenixDialect_jaydebeapidb(PhoenixDialect):
    _phoenix_driver = 'org.apache.phoenix.jdbc.PhoenixDriver'
    driver = "jaydebeapidb"

    @classmethod
    def dbapi(cls):
        from jpype import setUsePythonThreadForDeamon
        import jaydebeapi
        from jaydebeapi import _DEFAULT_CONVERTERS, _java_to_py
        _DEFAULT_CONVERTERS.update({'BIGINT': _java_to_py('longValue')})

        setUsePythonThreadForDeamon(False)

        return jaydebeapi

    def create_connect_args(self, url):
        opts = url.translate_connect_args()
        opts.update(url.query)

        if not opts.get('TenantId'):
            raise TenantError("Tenant id missing.")

        driver_url = 'jdbc:phoenix:' + opts['host'] + ':' + str(opts['port']) + ';TenantId=' + opts.get('TenantId')

        # return [self._phoenix_driver, driver_url, ], {'driver_args': [driver_url, '', ''], 'jars': opts['jars']}
        return [self._phoenix_driver, driver_url, {}, opts['jars']], {}


class TINYINT(types.Integer):
    __visit_name__ = "INTEGER"


class UTINYINT(types.Integer):
    __visit_name__ = "INTEGER"


class UINTEGER(types.Integer):
    __visit_name__ = "INTEGER"


class DOUBLE(types.BIGINT):
    __visit_name__ = "BIGINT"


class UDOUBLE(types.BIGINT):
    __visit_name__ = "BIGINT"


class UFLOAT(types.FLOAT):
    __visit_name__ = "FLOAT"


class ULONG(types.BIGINT):
    __visit_name__ = "BIGINT"


class UTIME(types.TIME):
    __visit_name__ = "TIME"


class UDATE(types.DATE):
    __visit_name__ = "DATE"


class UTIMESTAMP(types.TIMESTAMP):
    __visit_name__ = "TIMESTAMP"


class ROWID (types.String):
    __visit_name__ = "VARCHAR"


COLUMN_DATA_TYPE = {
    -6: TINYINT,
    -5: BIGINT,
    -3: VARBINARY,
    1: CHAR,
    3: DECIMAL,
    4: INTEGER,
    5: SMALLINT,
    6: FLOAT,
    8: DOUBLE,
    9: UINTEGER,
    10: ULONG,
    11: UTINYINT,
    12: VARCHAR,
    13: ROWID,
    14: UFLOAT,
    15: UDOUBLE,
    16: BOOLEAN,
    18: UTIME,
    19: UDATE,
    20: UTIMESTAMP,
    91: DATE,
    92: TIME,
    93: TIMESTAMP
}


